`timescale 1ns / 1ps
`include "bsc.svh"

module boundary_scan_cell(
  /* parallel input/output */
  input logic pi, 
  output logic po,
  
  /* serial input/output */
  input logic si, 
  output logic so,
  
  input logic clock,
  input logic update,
  
  input Bsc::input_mode_t input_mode,
  input Bsc::output_mode_t output_mode
    );
	 
 
  reg shift_register;
  reg latch_register;
  
  assign po = output_mode ? latch_register : pi;
  assign latch_register = update ? shift_register : latch_register;
  assign so = shift_register;
  
  always @ (posedge clock)
    begin
      shift_register <= input_mode ? si : pi;
    end
  
endmodule