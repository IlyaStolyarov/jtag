`ifndef BSC_H_INCLUDED
  `define BSC_H_INCLUDED
  
  package Bsc;
    typedef enum logic {
      LOAD = 1'b0,
      SHIFT = 1'b1
    } input_mode_t;

    typedef enum logic {
      NORMAL = 1'b0,
      TEST = 1'b1
    } output_mode_t;
  endpackage
  
`endif
