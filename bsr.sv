`timescale 1ns / 1ps

interface BoundaryScanRegister #(parameter width = 1) (
    input logic[width-1:0] test_in
  );

  logic[width-1:0] test_out;
  
  initial
    begin : WIDTH_CHECK
      $display("Width for %m is %0d", width);
      $display("Size of test_in for %m is %0d", $size(test_in));
    end

endinterface

module BoundaryScan (
  input logic in,
  output logic out,
  
  input logic clock,
  input logic update,
  
  input Bsc::input_mode_t input_mode,
  input Bsc::output_mode_t output_mode,
  
  BoundaryScanRegister register
  );

  wire bsc_interconnect[$size(register.test_in)-1:0];
  genvar i;
  generate
    for (i = 0; i<$size(bsc_interconnect); i++)
      begin : boundary_scan_register_generation
        if (i == 0)
          begin
          	boundary_scan_cell bsc_insts (
              .pi(register.test_in[i]),
              .po(register.test_out[i]),
              
              .si(in),
              .so(bsc_interconnect[i]),

              .clock,
              .update,

              .input_mode,
              .output_mode
            );
          end
        else
          begin
            boundary_scan_cell bsc_insts (
              .pi(register.test_in[i]),
              .po(register.test_out[i]),
              
              .si(bsc_interconnect[i-1]),
              .so(bsc_interconnect[i]),

              .clock,
              .update,

              .input_mode,
              .output_mode
            );
          end
        
        if (i == $size(bsc_interconnect)-1)
          begin
            assign out = bsc_interconnect[i];
          end
      end
  endgenerate
  
endmodule