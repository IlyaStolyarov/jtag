# for enhancing USB BlasterII to be reliable, 25MHz
create_clock -name {tck} -period 40 {tck}
set_input_delay -clock tck -clock_fall 3 [get_ports tdi]
set_input_delay -clock tck -clock_fall 3 [get_ports tms]
set_output_delay -clock tck 3 [get_ports tdo]