# Copyright (C) 2017  Intel Corporation. All rights reserved.
# Your use of Intel Corporation's design tools, logic functions 
# and other software and tools, and its AMPP partner logic 
# functions, and any output files from any of the foregoing 
# (including device programming or simulation files), and any 
# associated documentation or information are expressly subject 
# to the terms and conditions of the Intel Program License 
# Subscription Agreement, the Intel Quartus Prime License Agreement,
# the Intel FPGA IP License Agreement, or other applicable license
# agreement, including, without limitation, that your use is for
# the sole purpose of programming logic devices manufactured by
# Intel and sold by Intel or its authorized distributors.  Please
# refer to the applicable agreement for further details.

# Quartus Prime: Generate Tcl File for Project
# File: jtag_quartus.tcl
# Generated on: Fri Feb 09 00:54:26 2018

# Load Quartus Prime Tcl Project package
package require ::quartus::project

set need_to_close_project 0
set make_assignments 1

# Check that the right project is open
if {[is_project_open]} {
	if {[string compare $quartus(project) "jtag"]} {
		puts "Project jtag is not open"
		set make_assignments 0
	}
} else {
	# Only open if not already open
	if {[project_exists jtag]} {
		project_open -revision TestAccessPort jtag
	} else {
		project_new -revision TestAccessPort jtag
	}
	set need_to_close_project 1
}

# Make assignments
if {$make_assignments} {
	set_global_assignment -name FAMILY "MAX V"
	set_global_assignment -name DEVICE 5M570ZF256C5
	set_global_assignment -name ORIGINAL_QUARTUS_VERSION 17.1.1
	set_global_assignment -name PROJECT_CREATION_TIME_DATE "11:18:16  JANUARY 30, 2018"
	set_global_assignment -name LAST_QUARTUS_VERSION "17.1.1 Lite Edition"
	set_global_assignment -name PROJECT_OUTPUT_DIRECTORY output_files
	set_global_assignment -name MIN_CORE_JUNCTION_TEMP 0
	set_global_assignment -name MAX_CORE_JUNCTION_TEMP 85
	set_global_assignment -name ERROR_CHECK_FREQUENCY_DIVISOR "-1"
	set_global_assignment -name EDA_SIMULATION_TOOL "ModelSim-Altera (SystemVerilog)"
	set_global_assignment -name EDA_TIME_SCALE "1 ps" -section_id eda_simulation
	set_global_assignment -name EDA_OUTPUT_DATA_FORMAT "SYSTEMVERILOG HDL" -section_id eda_simulation
	set_global_assignment -name POWER_PRESET_COOLING_SOLUTION "NO HEAT SINK WITH STILL AIR"
	set_global_assignment -name ALWAYS_ENABLE_INPUT_BUFFERS ON
	set_global_assignment -name EDA_TEST_BENCH_ENABLE_STATUS TEST_BENCH_MODE -section_id eda_simulation
	set_global_assignment -name EDA_NATIVELINK_SIMULATION_TEST_BENCH DUT -section_id eda_simulation
	set_global_assignment -name EDA_TEST_BENCH_NAME DUT -section_id eda_simulation
	set_global_assignment -name EDA_DESIGN_INSTANCE_NAME NA -section_id DUT
	set_global_assignment -name EDA_TEST_BENCH_MODULE_NAME DUT -section_id DUT
	set_global_assignment -name EDA_TEST_BENCH_FILE ../testbench.sv -section_id DUT
	set_global_assignment -name VERILOG_INPUT_VERSION SYSTEMVERILOG_2005
	set_global_assignment -name VERILOG_SHOW_LMF_MAPPING_MESSAGES OFF
	set_global_assignment -name SOURCE_FILE ../registers.svh
	set_global_assignment -name SVF_FILE ../test.svf
	set_global_assignment -name SYSTEMVERILOG_FILE ../registers.sv
	set_global_assignment -name SYSTEMVERILOG_FILE ../testbench.sv
	set_global_assignment -name SOURCE_FILE ../tap.svh
	set_global_assignment -name SYSTEMVERILOG_FILE ../tap.sv
	set_global_assignment -name SYSTEMVERILOG_FILE ../bsr.sv
	set_global_assignment -name SOURCE_FILE ../bsc.svh
	set_global_assignment -name SYSTEMVERILOG_FILE ../bsc.sv
	set_global_assignment -name SDC_FILE ../constraints.sdc
	set_location_assignment PIN_P2 -to tdi
	set_location_assignment PIN_M4 -to tdo
	set_location_assignment PIN_L4 -to tms
	set_location_assignment PIN_N3 -to tck
	set_location_assignment PIN_R1 -to led1
	set_location_assignment PIN_P4 -to led0
	set_location_assignment PIN_C2 -to vcc_target

	# Including default assignments
	set_global_assignment -name TIMEQUEST_MULTICORNER_ANALYSIS OFF -family "MAX V"
	set_global_assignment -name TIMEQUEST_REPORT_WORST_CASE_TIMING_PATHS ON -family "MAX V"
	set_global_assignment -name TIMEQUEST_CCPP_TRADEOFF_TOLERANCE 0 -family "MAX V"
	set_global_assignment -name TDC_CCPP_TRADEOFF_TOLERANCE 0 -family "MAX V"
	set_global_assignment -name TIMEQUEST_DO_CCPP_REMOVAL OFF -family "MAX V"
	set_global_assignment -name TIMEQUEST_SPECTRA_Q OFF -family "MAX V"
	set_global_assignment -name SYNCHRONIZATION_REGISTER_CHAIN_LENGTH 2 -family "MAX V"
	set_global_assignment -name OPTIMIZE_HOLD_TIMING "IO PATHS AND MINIMUM TPD PATHS" -family "MAX V"
	set_global_assignment -name OPTIMIZE_MULTI_CORNER_TIMING OFF -family "MAX V"
	set_global_assignment -name AUTO_DELAY_CHAINS ON -family "MAX V"
	set_global_assignment -name USE_CONFIGURATION_DEVICE ON -family "MAX V"

	# Commit assignments
	export_assignments

	# Close project
	if {$need_to_close_project} {
		project_close
	}
}
