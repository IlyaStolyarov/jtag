`timescale 1ns / 1ps

`define ID_CODE_VER 4'b1
`define ID_CODE_PART_NUM 16'd0
`define ID_CODE_MANUFACTURER 11'b0
`define ID_CODE_RESERVED 1'b1

`include "registers.svh"

module IdCodeRegister (
  input logic tck,
  input logic tdi,
  output logic tdo,
  
  input logic shift,
  input logic reset
  );
  
	parameter idcode = {`ID_CODE_VER,`ID_CODE_PART_NUM,`ID_CODE_MANUFACTURER,`ID_CODE_RESERVED};
  reg [31:0] id = idcode;
  
  always @ (negedge tck)
  begin
    if (reset) id <= idcode;

		if (!reset && shift)
		begin
			tdo <= id[$low(id)];
			id <= (id >> 1);
			id[$high(id)] <= tdi;
		end
  end
  
endmodule

module BypassRegister (
  input logic tck,
  input logic tdi,
  output logic tdo
  );
  
  reg bypass;
  
  always @ (negedge tck)
  begin
    bypass <= tdi;
    tdo <= bypass;
  end 
  
endmodule

module InstructionRegister (
  input logic tck,
  input logic tdi,
  output logic tdo,
  
  input logic shift,
  output Reg::InstructionCode ir_code
  );
  
  reg [3:0] ir = Reg::BYPASS;
  assign ir_code = Reg::InstructionCode'(ir);
  
  always @ (negedge tck)
  begin
    if (shift)
    begin
      tdo <= ir[$low(ir)];
      ir <= (ir >> 1);
      ir[$high(ir)] <= tdi;
      
      // #1 $display ("IR tdo: %b", tdo);
    end
  end
endmodule