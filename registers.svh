`ifndef REGISTERS_H_INCLUDED
`define REGISTERS_H_INCLUDED

package Reg;
  typedef enum reg [3:0] {
          BYPASS = 4'b1000,
          SAMPLE = 4'b0001,
          PRELOAD = 4'b0010,
          EXTEST = 4'b0000,
          IDCODE = 4'b1110
      } InstructionCode;
endpackage

package RegControl;
   struct {
     logic tdi;
     logic tdo;
  } Bypass;
  
  struct {
    logic tdi;
    logic tdo;
    
    logic shift;
    Reg::InstructionCode ir;
  } Instruction;
  
  struct {
    logic tdi;
    logic tdo;
    
    logic shift;
    logic reset;
 } IdCode;
 
  struct {
    logic in;
    logic out;
    logic latch;
    Bsc::input_mode_t input_mode;
    Bsc::output_mode_t output_mode;
  } BoundaryScan;
endpackage

`endif