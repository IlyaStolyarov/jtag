`timescale 1ns / 1ps

`include "tap.svh"
`include "registers.svh"

parameter cells_to_test = 8;
reg [cells_to_test-1:0] test_data = 8'b01011010;

module TapState (
  input tck,
  input tms,
  
  output Tap::ControllerState controllerState = Tap::RESET
  );
   
  always @ (posedge tck)
  begin : TAP_FSM
    case (controllerState)
			Tap::RESET:           controllerState <= tms ? Tap::RESET          : Tap::IDLE;
			Tap::IDLE:            controllerState <= tms ? Tap::SELECT_DR_SCAN : Tap::IDLE;

			Tap::SELECT_DR_SCAN:  controllerState <= tms ? Tap::SELECT_IR_SCAN : Tap::CAPTURE_DR;
			Tap::CAPTURE_DR:      controllerState <= tms ? Tap::EXIT1_DR       : Tap::SHIFT_DR;
			Tap::SHIFT_DR:        controllerState <= tms ? Tap::EXIT1_DR       : Tap::SHIFT_DR;
			Tap::EXIT1_DR:        controllerState <= tms ? Tap::UPDATE_DR      : Tap::PAUSE_DR;
			Tap::PAUSE_DR:        controllerState <= tms ? Tap::EXIT2_DR       : Tap::PAUSE_DR;
			Tap::EXIT2_DR:        controllerState <= tms ? Tap::PAUSE_DR       : Tap::EXIT1_DR;
			Tap::UPDATE_DR:       controllerState <= tms ? Tap::SELECT_DR_SCAN : Tap::IDLE;

			Tap::SELECT_IR_SCAN:  controllerState <= tms ? Tap::RESET          : Tap::CAPTURE_IR;
			Tap::CAPTURE_IR:      controllerState <= tms ? Tap::EXIT1_IR       : Tap::SHIFT_IR;
			Tap::SHIFT_IR:        controllerState <= tms ? Tap::EXIT1_IR       : Tap::SHIFT_IR;
			Tap::EXIT1_IR:        controllerState <= tms ? Tap::UPDATE_IR      : Tap::PAUSE_IR;
			Tap::PAUSE_IR:        controllerState <= tms ? Tap::EXIT2_IR       : Tap::PAUSE_IR;
			Tap::EXIT2_IR:        controllerState <= tms ? Tap::PAUSE_IR       : Tap::EXIT1_IR;
			Tap::UPDATE_IR:       controllerState <= tms ? Tap::SELECT_DR_SCAN : Tap::IDLE;
    endcase
  end
endmodule

module BoundaryModeSwitcher (
    input tck,   
    input Reg::InstructionCode selected_instruction,
  
    output Bsc::input_mode_t input_mode,
    output Bsc::output_mode_t output_mode
  );
 
  always @ (negedge tck)
  begin : BSR_FSM_ACTIONS
  if ((selected_instruction == Reg::PRELOAD) ||
      (selected_instruction == Reg::SAMPLE) ||
      (selected_instruction == Reg::EXTEST))
    begin  
      case (selected_instruction)
        Reg::PRELOAD:
          begin
            input_mode  <= Bsc::SHIFT;
            output_mode <= Bsc::NORMAL;
          end
        Reg::SAMPLE:
          begin
            input_mode  <= Bsc::LOAD;
            output_mode <= Bsc::NORMAL;
          end
        Reg::EXTEST:
          begin
            input_mode  <= Bsc::SHIFT;
            output_mode <= Bsc::TEST;
          end
        endcase
    end
  end
 
endmodule

module RegisterSwitcher (
    /* Primary wires to connect */
    input    logic tdi,
    output   logic tdo,
    
    /* Conditions for switching wires */
    input    Tap::ControllerState controllerState,
    input    Reg::InstructionCode ir_code,
    
    /* Interconnect wires */    
    output   logic instruction_tdi,
    input    logic instruction_tdo,

    output   logic bypass_tdi,
    input    logic bypass_tdo,

    output   logic idcode_tdi,
    input    logic idcode_tdo,

    output   logic boundary_scan_tdi,
    input    logic boundary_scan_tdo,
    
    
    /* Control wires */
    output   logic instruction_shift,
        
    output   logic idcode_shift,
    output   logic idcode_reset
  );

always_latch begin
  if (controllerState == Tap::RESET)
  begin
    bypass_tdi = tdi;
    tdo = bypass_tdo;
  end

  if (controllerState == Tap::SHIFT_IR)
  begin
    instruction_tdi = tdi;
    tdo = instruction_tdo;
  end

  if (controllerState == Tap::SHIFT_DR)
  begin
    if (ir_code == Reg::BYPASS)
    begin
      bypass_tdi = tdi;
      tdo = bypass_tdo;
    end
      
    if (ir_code == Reg::IDCODE)
    begin              
      idcode_tdi = tdi;
      tdo = idcode_tdo;
    end
    
    if ((ir_code == Reg::PRELOAD) ||
        (ir_code == Reg::SAMPLE)  ||
        (ir_code == Reg::EXTEST))
    begin
      boundary_scan_tdi = tdi;
      tdo = boundary_scan_tdo;
    end
  end
end
 
/* Conditions for control wires */
always_comb begin
    instruction_shift = (controllerState == Tap::SHIFT_IR) ? 1'b1 : 1'b0;
    idcode_shift = ((ir_code == Reg::IDCODE) && (controllerState == Tap::SHIFT_DR)) ? 1'b1 : 1'b0;
    idcode_reset = (controllerState == Tap::UPDATE_IR) ? 1'b1 : 1'b0;
end

endmodule

module TestAccessPort (
  input  logic tdi,
  output logic tdo,
  input  logic tms,
  input  logic tck,

  output logic led0,
  output logic led1,
	 
  output logic vcc_target
  );
		
	assign led0 = tms;
	assign led1 = tdo;
	
	assign vcc_target = 1;
	
	Tap::ControllerState controllerState;
	TapState tap_state(.tck, .tms, .controllerState);
	
  BoundaryScanRegister #(.width(cells_to_test)) bsr (.test_in(test_data));
  
  InstructionRegister inctruction_register(
    .tck(tck),
    .tdi(RegControl::Instruction.tdi),
    .tdo(RegControl::Instruction.tdo),
    .shift(RegControl::Instruction.shift),
    .ir_code(RegControl::Instruction.ir)
  );
                                                 
  BypassRegister bypass_register(
    .tck(tck),
    .tdi(RegControl::Bypass.tdi),
    .tdo(RegControl::Bypass.tdo)
  );
  
  IdCodeRegister idcode_register(
    .tck(tck),
    .tdi(RegControl::IdCode.tdi),
    .tdo(RegControl::IdCode.tdo),
    .shift(RegControl::IdCode.shift),
    .reset(RegControl::IdCode.reset)
  );
  
  BoundaryModeSwitcher boundary_mode_switcher(
    .tck,    
    .selected_instruction(RegControl::Instruction.ir),
    
    .input_mode(RegControl::BoundaryScan.input_mode),
    .output_mode(RegControl::BoundaryScan.output_mode)
  );
  
  BoundaryScan boundary_scan(
    .in(RegControl::BoundaryScan.in),
    .out(RegControl::BoundaryScan.out),
    .clock(tck),
    .update(RegControl::BoundaryScan.latch),
    .input_mode(RegControl::BoundaryScan.input_mode),
    .output_mode(RegControl::BoundaryScan.output_mode),
    .register(bsr)
  );
  
RegisterSwitcher register_switcher (
      .tdi,
      .tdo,
      .controllerState,
      .ir_code(RegControl::Instruction.ir),
      
      .instruction_tdi(RegControl::Instruction.tdi),
      .instruction_tdo(RegControl::Instruction.tdo),
      .instruction_shift(RegControl::Instruction.shift),
  
      .bypass_tdi(RegControl::Bypass.tdi),
      .bypass_tdo(RegControl::Bypass.tdo),
  
      .idcode_tdi(RegControl::IdCode.tdi),
      .idcode_tdo(RegControl::IdCode.tdo),
      .idcode_shift(RegControl::IdCode.shift),
      .idcode_reset(RegControl::IdCode.reset),
      
      .boundary_scan_tdi(RegControl::BoundaryScan.in),
      .boundary_scan_tdo(RegControl::BoundaryScan.out)
    );

`ifdef DEBUG
  always @ (controllerState)
  begin : TAP_FSM_DEBUG
    case (controllerState)
      RESET:          $display("TAP state: RESET");
      IDLE:           $display("TAP state: IDLE");
      
      SELECT_DR_SCAN: $display("TAP state: SELECT_DR_SCAN");
      CAPTURE_DR:     $display("TAP state: CAPTURE_DR");
      SHIFT_DR:       $display("TAP state: SHIFT_DR");
      EXIT1_DR:       $display("TAP state: EXIT1_DR");
      PAUSE_DR:       $display("TAP state: PAUSE_DR");
      EXIT2_DR:       $display("TAP state: EXIT2_DR");
      UPDATE_DR:      $display("TAP state: UPDATE_DR");

      SELECT_IR_SCAN: $display("TAP state: SELECT_IR_SCAN");
      CAPTURE_IR:     $display("TAP state: CAPTURE_IR");
      SHIFT_IR:       $display("TAP state: SHIFT_IR");
      EXIT1_IR:       $display("TAP state: EXIT1_IR");
      PAUSE_IR:       $display("TAP state: PAUSE_IR");
      EXIT2_IR:       $display("TAP state: EXIT2_IR");
      UPDATE_IR:      $display("TAP state: UPDATE_IR");
    endcase
  end
  
//  static Reg [1:0] i = 0;
//  static ControllerState prevState = RESET;
//  always @ (negedge tck)
//	begin : LED_BLINKER
//		if (prevState != controllerState)
//		begin
//			led0 <= i[$low(i)];
//			led1 <= i[$high(i)];
//			i++;
//			#1 $display("LED0: %d, LED1: %d", led0, led1);
//		end
//		prevState = controllerState;
//	end
`endif

endmodule
 