`ifndef TAP_H_INCLUDED
`define TAP_H_INCLUDED
  
package Tap;
	typedef enum reg [3:0] {
		RESET,
		IDLE,

		SELECT_DR_SCAN,
		CAPTURE_DR,
		SHIFT_DR,
		EXIT1_DR,
		PAUSE_DR,
		EXIT2_DR,
		UPDATE_DR,

		SELECT_IR_SCAN,
		CAPTURE_IR,
		SHIFT_IR,
		EXIT1_IR,
		PAUSE_IR,
		EXIT2_IR,
		UPDATE_IR
	} ControllerState;
 endpackage
    
`endif