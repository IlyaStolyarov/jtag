`timescale 1ns / 1ps

module DUT;

  reg tdi;
  reg tdo;
  reg tms;
  reg tck;
		 
  reg led0;
  reg led1;
  
  reg vcc_target;
   
  // Instantiate Design Under Test
  TestAccessPort TAP (
    .tdi,
    .tdo,
    .tms,
    .tck,
		
		.led0,
	  .led1,
		
	  .vcc_target
  );

  /* 40ns ought to be enough for anybody (25MHz) */
  parameter clock_period = 250; 
  reg [31:0] tdo_look = 0;
  
  task load_instruction;
    input Reg::InstructionCode instruction_code;
    
    automatic reg [3:0] i = 0;
    begin
      // TEST_LOGIC_RESET
      tms = 1'b1;
      #(5*clock_period)
      assert(DUT.TAP.controllerState == Tap::RESET);

      // RUN_TEST/IDLE
      tms = 1'b0;
      #(1*clock_period)
      assert(DUT.TAP.controllerState == Tap::IDLE);

      // SELEST DR-SCAN
      tms = 1'b1;
      #(1*clock_period)
      assert(DUT.TAP.controllerState == Tap::SELECT_DR_SCAN);

      // SELEST IR-SCAN
      tms = 1'b1;
      #(1*clock_period)
      assert(DUT.TAP.controllerState == Tap::SELECT_IR_SCAN);

      // CAPTURE-IR
      tms = 1'b0;
      #(1*clock_period)
      assert(DUT.TAP.controllerState == Tap::CAPTURE_IR);

      // SHIFT_IR
      while (i < $size(instruction_code))
        begin
          tms = 1'b0;
          tdi = instruction_code[i];
          #(1*clock_period) i = i + 1;
          $display("%d, %d", tdi, tdo);
        end
      tdi = 1'b0;

      assert(DUT.TAP.controllerState == Tap::SHIFT_IR);

      // EXIT1-IR
      tms = 1'b1;
      #(1*clock_period)
      assert(DUT.TAP.controllerState == Tap::EXIT1_IR);
      
      `ifndef XILINX_SIMULATOR
        $display("Returned IR: %b", tdo_look[$high(tdo_look):$size(tdo_look)-$size(instruction_code)]);
      `else
        $display("Returned IR: %b", tdo_look[4:0]); // More simple line versus line above
      `endif
      
      // UPDATE-IR
      tms = 1'b1;
      #(1*clock_period)
      assert(DUT.TAP.controllerState == Tap::UPDATE_IR);

      // SELEST DR-SCAN
      tms = 1'b1;
      #(1*clock_period)
      assert(DUT.TAP.controllerState == Tap::SELECT_DR_SCAN);

      // SELEST IR-SCAN
      tms = 1'b1;
      #(1*clock_period)
      assert(DUT.TAP.controllerState == Tap::SELECT_IR_SCAN);

      // TEST_LOGIC_RESET
      tms = 1'b1;
      #(1*clock_period)
      assert(DUT.TAP.controllerState == Tap::RESET);
    end
  endtask
  
  task read_data;
    input integer length;
    
    begin
      // RUN_TEST/IDLE
      tms = 1'b0;
      #(1*clock_period)
      assert(DUT.TAP.controllerState == Tap::IDLE);
      
      // SELEST DR-SCAN
      tms = 1'b1;
      #(1*clock_period)
      assert(DUT.TAP.controllerState == Tap::SELECT_DR_SCAN);
      
      // CAPTURE-DR
      tms = 1'b0;
      #(1*clock_period)
      assert(DUT.TAP.controllerState == Tap::CAPTURE_DR);
      
      // SHIFT_DR
      tms = 1'b0;
      $display("Reading %d bits", length);
      #(length*clock_period)
      assert(DUT.TAP.controllerState == Tap::SHIFT_DR);
      
      // EXIT1-DR
      tms = 1'b1;
      #(1*clock_period)
      assert(DUT.TAP.controllerState == Tap::EXIT1_DR);
      
      $display("%b", tdo_look);
      
      // UPDATE-DR
      tms = 1'b1;
      #(1*clock_period)
      assert(DUT.TAP.controllerState == Tap::UPDATE_DR);
      
      // SELEST DR-SCAN
      tms = 1'b1;
      #(1*clock_period)
      assert(DUT.TAP.controllerState == Tap::SELECT_DR_SCAN);
      
      // SELEST IR-SCAN
      tms = 1'b1;
      #(1*clock_period)
      assert(DUT.TAP.controllerState == Tap::SELECT_IR_SCAN);
      
      // TEST_LOGIC_RESET
      tms = 1'b1;
      #(1*clock_period)
      assert(DUT.TAP.controllerState == Tap::RESET);
    end
  endtask
    
  
  always #(clock_period/'d2) tck=~tck;
  
  always @ (posedge tck)
  begin : TDO_DEBUG_LOOOKUP
    tdo_look = tdo_look >> 1;
    tdo_look[$high(tdo_look)] = tdo;
  end

  initial
  begin : main_test_process
    tdi = 0;
    tms = 0;
    tck = 0;
    #(1*clock_period/4) // sync
    
    // TEST_LOGIC_RESET
    tms = 1'b1;
    #(5*clock_period)

    $display("Start testbench");
    assert(DUT.TAP.controllerState == Tap::RESET);

    $display("Load instruction IDCODE");
    load_instruction(Reg::IDCODE);
    assert(DUT.TAP.inctruction_register.ir == Reg::IDCODE);
    
    $display("Read data");
    read_data(32);
    
    $display("Load instruction SAMPLE");
    load_instruction(Reg::SAMPLE);
    assert(DUT.TAP.inctruction_register.ir == Reg::SAMPLE);
    
    $display("Read data");
    read_data(8);
 
    $finish;
  end
  
  initial
  begin : WAVE_DUMP_DEBUG
    $dumpfile("dump.vcd");
    $dumpvars;
  end

endmodule